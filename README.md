# android-expandinglayout
Small Android Library that allows you to expand / hide views using the default LinearLayout and Animations.

Simple Custom View I designed at work, similar to the Android Contact screen. Logic for handling what happens after expanding / hiding is delegated to the caller. 
